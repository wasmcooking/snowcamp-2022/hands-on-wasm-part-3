
const fancy = require('./fancy')

const childName = fancy.fancyName()

process.on("message", (message) => {

  console.log(`🐥> message received from (Mum)parent process:`, message)

  // answer
  process.send({
    text: "🐥> 👋 hello from Child",
    pid: process.pid,
    name: childName
  })

  // Add a loop and send messages
  setInterval(() =>{ 
    process.send({
      text: `ping by ${process.pid} aka ${childName}`
    })
  }, 2000) //run this thang every 2 seconds

})

process.on("exit", (code) => {
  console.log(`👋> exit code:`, code)
})

process.on('SIGTERM', (code) => {
  console.log(`😡> exit code:`, code)
  process.exit() 
}) // catch kill
