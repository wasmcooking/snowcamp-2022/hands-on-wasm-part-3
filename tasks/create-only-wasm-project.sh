#!/bin/bash
if [[ $# -eq 0 ]] ; then
    echo '😡 you must give a project name'
    exit 0
fi

directory_name=$1
go_version="1.17"

cp templates/only-wasm/ ${directory_name} -r

# Create go.mod
cat > ${directory_name}/go.mod <<- EOM
module ${directory_name}

go ${go_version}
EOM

# Create README.md
cat > ${directory_name}/README.md <<- EOM
# ${directory_name}

EOM

# Download wasm_exec.js
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./${directory_name}

