# Tiny FaaS (Go version)

- Build the 2 functions `hello` and `hey`
- Copy the wasm files to the `./wasm` directory

## Create the child process to load and run the wasm file

Create the `child.js` file

```javascript
const fs = require('fs')
require("./wasm_exec")

async function runWasm(wasmFile, args) {
  const go = new Go()
  try {
    let { instance } = await WebAssembly.instantiate(wasmFile, go.importObject)
    if(args) go.argv = args
    go.run(instance) 
  } catch(err) {
    throw err
  }
}

const loadWasmFile = async (pathToWasmFile) => {
  const wasmFile = fs.readFileSync(pathToWasmFile)
  await runWasm(wasmFile)
  // new function(s) available
  // HandleRequest
}

process.on("message", async (message) => {

  console.log("[child] message received from parent process:", message)

  switch (message.cmd) {
    case "load-wasm":
      
      await loadWasmFile(message.pathToWasmFile)

      console.log("[child] load:", process.pid)

      process.send({
        pid: process.pid, 
      })
      break;
    
    case "call-function":
      console.log("[child] calling function")
      // Call GoLang Handle() function
      let result = Handle(message.arguments)
      // Then return result
      process.send({
        result: result,
      })
      break;
      
    default:
      break;
  }

})
```

Create the `index.js` file

```javascript
// Fastify
let fastifyServerOptions = {
  logger: process.env.LOGGING || true
}
const fastify = require('fastify')(fastifyServerOptions)

const fs = require('fs')
const path = require('path')
const fork = require('child_process').fork

const wasmPath = './wasm'
let wasmExecutorsMap = new Map()
let counter = 0


let wasmFiles = fs.readdirSync(wasmPath).filter(file=>path.extname(file)==='.wasm')

wasmFiles.forEach(wasmFile => {

  let childProcess = fork("./child.js")
  
  childProcess.once("message", message => {

    if(message.pid) {

      let wasmExecutor = Object.assign(message, { childProcess: childProcess})

      wasmExecutorsMap.set(
        path.parse(wasmFile).name,
        wasmExecutor
      )
      
      counter++
      // when all wasm are loaded
      if(counter==wasmFiles.length) {

        console.log("[parent] all functions are loaded")

        startService()
      }
    }
  })
  
  childProcess.send({
    cmd:"load-wasm",
    pathToWasmFile: `${wasmPath}/${wasmFile}`
  })

})

let cleanExit = () => { 
  wasmExecutorsMap.forEach(executor => {
    executor.childProcess.kill()
  })
  process.exit() 
}
process.on('SIGINT', cleanExit); // catch ctrl-c
process.on('SIGTERM', cleanExit); // catch kill


// Declare a route
fastify.post('/functions/call/:function_name', async (request, reply) => {
  
  let jsonParameters = request.body
  let functionName = request.params.function_name

  childProcess = wasmExecutorsMap.get(functionName).childProcess

  childProcess.once("message", message => {
    if(message.result) {
      reply.header('Content-Type', 'application/json; charset=utf-8').send({success: message.result}) 
    } else {
      reply.code(500).send({failure: "error"})
    }
  })

  childProcess.send({
    cmd:"call-function",
    arguments: jsonParameters
  })

  await reply
})


const startService = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}
```

Run the application server:

```bash
node index.js
```

Try this in another terminal:

```bash
ps -d
```