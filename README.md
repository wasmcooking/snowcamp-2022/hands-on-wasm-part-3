# Hands-on: wasm discovery


## Gitpod

This project is a [Gitpod](https://gitpod.io) project: if you open this project with GitPod, you'll get an entire GoLang & TinyGo toolchain.

## Tools

If you want to generate a **Go WASM** project for the **browser**, run the VSCode Task: `Tasks: Run Task`, choose `generate go web project` and give a name to the project (it will generate a new directory).

If you want to generate a **TinyGo WASM** project for the **browser**, run the VSCode Task: `Tasks: Run Task`, choose `generate go web project` and give a name to the project (it will generate a new directory).



