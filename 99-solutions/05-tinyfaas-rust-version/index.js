/**
 * @typedef WasmExecutor
 * @type {object}
 * @property {number} pid - process id
 * @property {string} name - function name
 * @property {string} version - function version ex: 0.0.0
 * @property {ChildProcess} childProcess - fork
 */

/**
 * @typedef WasmExecutorsMap
 * @type {Map<string,WasmExecutor>}
 */

/**
 * @typedef JsonRustResult
 * @type {object}
 */

/**
 * @typedef OnLoadResponseMessage
 * @type {object}
 * @property {number} pid - process id
 */

/**
 * @typedef OnResultResponseMessage
 * @type {object}
 * @property {number} pid - process id
 * @property {JsonRustResult} result - result of the function 
 */

let fastifyServerOptions = {
  logger: process.env.LOGGING || true
}
const fastify = require('fastify')(fastifyServerOptions)

const fs = require('fs')
const path = require('path')
const fork = require('child_process').fork

const wasmPath = './wasm'
/** @type {WasmExecutorsMap} */
let wasmExecutorsMap = new Map()
let counter = 0


let wasmFiles = fs.readdirSync(wasmPath).filter(file=>path.extname(file)==='.js') // 👋

wasmFiles.forEach(wasmFile => {

  let childProcess = fork("./child.js")
  
  childProcess.once("message", /** @param {OnLoadResponseMessage} message */ message => {

    if(message.pid) {
      /** @type {WasmExecutor} */
      let wasmExecutor = Object.assign(message, { childProcess: childProcess})

      wasmExecutorsMap.set(
        path.parse(wasmFile).name,
        wasmExecutor
      )
      
      counter++
      // when all wasm are loaded
      if(counter==wasmFiles.length) {

        console.log("[parent] all functions are loaded")

        startService()
      }
    }
  })
  
  childProcess.send({
    cmd:"load-wasm",
    pathToWasmFile: `${wasmPath}/${wasmFile}`
  })

})

let cleanExit = () => { 
  wasmExecutorsMap.forEach(executor => {
    executor.childProcess.kill()
  })
  process.exit() 
}
process.on('SIGINT', cleanExit); // catch ctrl-c
process.on('SIGTERM', cleanExit); // catch kill


// Declare a route
fastify.post('/functions/call/:function_name', async (request, reply) => {
  
  let jsonParameters = request.body
  let functionName = request.params.function_name

  childProcess = wasmExecutorsMap.get(functionName).childProcess

  childProcess.once("message", /** @param {OnResultResponseMessage} message */ message => {
    if(message.result) {
      reply.header('Content-Type', 'application/json; charset=utf-8').send({success: message.result}) 
    } else {
      reply.code(500).send({failure: "error"})
    }
  })

  childProcess.send({
    cmd:"call-function",
    arguments: jsonParameters
  })

  await reply
})


const startService = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}




/*
```bash
url_api="http://localhost:8080"
function_name="hello"
function_version="0.0.0"
data='{"firstName":"Bob", "lastName":"Morane"}'
curl -d "${data}" \
  -H "Content-Type: application/json" \
  -X POST "${url_api}/functions/call/${function_name}/${function_version}"
```

```bash
url_api="http://localhost:8080"
function_name="hey"
function_version="0.0.1"
data='{"firstName":"Bob", "lastName":"Morane"}'
curl -d "${data}" \
  -H "Content-Type: application/json" \
  -X POST "${url_api}/functions/call/${function_name}/${function_version}"
```
*/



