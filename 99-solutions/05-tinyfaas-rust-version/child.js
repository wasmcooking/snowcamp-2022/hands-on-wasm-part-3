const fs = require('fs')

/**
 * @typedef JsonRustResult
 * @type {object}
 */

/** This is the Command Message
 * @typedef CommandMessage
 * @type {object}
 * @property {string} cmd "load-wasm" or "call-function"
 * @property {string} pathToWasmFile only if cmd == "load-wasm"
 * @property {object} arguments only if cmd == "call-function"
 */

process.on("message", /** @param {CommandMessage} message */ async (message) => {

  console.log("[child] message received from parent process:", message)

  switch (message.cmd) {
    case "load-wasm":

      global.wasm = require(message.pathToWasmFile)
      
      console.log("[child] load:", process.pid)

      process.send({
        pid: process.pid, 
      })
      break;
    
    case "call-function":
      console.log("[child] calling function")
      // Call Rust handle() function
      /** @type {JsonRustResult} */
      let result = wasm.handle(message.arguments)
      // Then return result
      process.send({
        result: result,
      })
      break;
      
    default:
      break;
  }

})

