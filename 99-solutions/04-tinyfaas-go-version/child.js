const fs = require('fs')
require("./wasm_exec")

/**
 * @typedef JsonGoResult
 * @type {object}
 */

/** This is the Command Message
 * @typedef CommandMessage
 * @type {object}
 * @property {string} cmd "load-wasm" or "call-function"
 * @property {string} pathToWasmFile only if cmd == "load-wasm"
 * @property {object} arguments only if cmd == "call-function"
 */

/**
 * @param {Buffer} wasmFile 
 * @param {Array} args 
 */
async function runWasm(wasmFile, args) {
  const go = new Go()
  try {
    let { instance } = await WebAssembly.instantiate(wasmFile, go.importObject)
    if(args) go.argv = args
    go.run(instance) 
  } catch(err) {
    throw err
  }
}

/** Load the wasm file, then execute the GoLang main function
 * @param {string} pathToWasmFile ex: `./wasm/hello.wasm`
 * @returns {void}
 */
const loadWasmFile = async (pathToWasmFile) => {
  const wasmFile = fs.readFileSync(pathToWasmFile)
  await runWasm(wasmFile)
  // new function(s) available
  // HandleRequest
}

process.on("message", /** @param {CommandMessage} message */ async (message) => {

  console.log("[child] message received from parent process:", message)

  switch (message.cmd) {
    case "load-wasm":
      
      await loadWasmFile(message.pathToWasmFile)

      console.log("[child] load:", process.pid)

      process.send({
        pid: process.pid, 
      })
      break;
    
    case "call-function":
      console.log("[child] calling function")
      // Call GoLang Handle() function
      /** @type {JsonGoResult} */
      let result = Handle(message.arguments)
      // Then return result
      process.send({
        result: result,
      })
      break;
      
    default:
      break;
  }

})

